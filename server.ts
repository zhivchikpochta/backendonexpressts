import bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import cookieParser from 'cookie-parser';
import express, { request } from 'express';
const authRouter = require('./src/routes/authRouter');
const renderRouter = require('./src/routes/renderRouter');
const path = require('path');
const session = require('express-session');

// Временные импорты


// dotenv.config();
const app = express();
const port = 5000;
async function serverSart() {
  const app = express();
  app.set('view engine', 'pug');
  app.use(require('morgan')('dev'));
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(require('cors')());
  app.use(cookieParser())
  app.use(express.static(path.join(__dirname, 'public')))
  app.use(session({
  cookie: { maxAge: 60000 },
  secret: 'codeworkrsecret',
  saveUninitialized: false,
  resave: false
}));
  // прописать роутер
  app.use('/', authRouter);
  app.use('/', renderRouter);
  app.listen(port, () => {
    console.log(`Приложение запущено по адресу: http://localhost:${port}/`);
  });
}
serverSart();
