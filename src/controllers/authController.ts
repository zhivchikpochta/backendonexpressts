import bcrypt from 'bcrypt';
import express, { Request, Response } from 'express';
import { get } from 'https';
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator')
const {secret} = require("../../config")
// const getPrisma = require('../models/prisma')
import { PrismaClient} from '@prisma/client'
// const User = require('../models/User')



class authController {
    async reg(req:Request, res:Response){
        try{
            const prisma = new PrismaClient();
            const {email, password}:any = req.body;
            const candidate = await prisma.user.findUnique({
                where: {
                  email: email,
                },
              })
            if (candidate) {
                return res.status(400).json({message: "Пользователь с таким именем уже существует"})
            }
            const hashPassword = bcrypt.hashSync(password, 7);
            await prisma.user.create({
                data:{
                  email:req.body.email,
                  firstName:req.body.firstName,
                  lastName:req.body.lastName,
                  password:hashPassword
                }
              })
              await prisma.$disconnect()
              res.redirect('/login')
        } catch (e) {

        }
    }
    async login(req:Request, res:Response){
        try{
            const prisma = new PrismaClient();
            const {email, password}:any = req.body;
            // Ищем поьзователя с похожим @mail
            const user = await prisma.user.findUnique({
                where: {
                  email: email,
                },
              })
            // Проверяем есть-ли похожий пользователь ?
            if (!user) {
                return res.status(400).json({message: `Пользователь ${email} не найден`})
            }
            // Проверяем пароли
            const passwordDB:any = user.password
            const validPassword = bcrypt.compareSync(password, passwordDB)
            if (!validPassword) {
                return res.status(400).json({message: `Введен неверный пароль`})
            }
            // Цепляем JWT токен
            const id:any = user.id
            const generateAccessToken = (id:any, email:any) => {
                const payload = {
                    id,
                    email
                }
                return jwt.sign(payload, secret, {expiresIn: "1h"} )
            }
            const token = generateAccessToken(id,email);
            console.log(token)
            return res.json({token})
        } catch (e) {

        }
    }
}
module.exports = new authController()