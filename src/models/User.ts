import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import express, { Request, Response } from 'express';
import { PrismaClient} from '@prisma/client'
const getPrisma = require('../models/prisma')
/*
export  async function createUser(req:Request,res:Response) {
    await prisma.user.create({
      data:{
        email:req.body.email,
        firstName:req.body.firstName,
        lastName:req.body.lastName,
        password:req.body.password
      }
    })
    await prisma.$disconnect()
    res.redirect('/login')
  }
*/
class User {
  async createUser(req:Request,res:Response,hashPassword:string ){
      await getPrisma.user.create({
        data:{
          email:req.body.email,
          firstName:req.body.firstName,
          lastName:req.body.lastName,
          password:hashPassword
        }
      })
      await getPrisma.$disconnect()
      res.redirect('/login')

  }
}
module.exports = User