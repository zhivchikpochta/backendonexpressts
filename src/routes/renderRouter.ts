const Router = require('express')



const renderController = require('../controllers/renderController')

const  renderRouter = new Router();
// Публичные страницы

renderRouter.get('/', renderController.index);
renderRouter.get('/reg', renderController.reg);
renderRouter.get('/login', renderController.login);
renderRouter.get('/items', renderController.items);
renderRouter.get('/items/:uid', renderController.itemsuid);
renderRouter.get('/users', renderController.users);
renderRouter.get('/users/:id', renderController.usersid);

// Страницы для авторизированного пользователя

renderRouter.get('/item_create', renderController.item_create);
renderRouter.get('/user', renderController.user);
renderRouter.get('/user/items', renderController.items);
renderRouter.get('/user/items/:uid', renderController.useritemsuid);

module.exports = renderRouter;