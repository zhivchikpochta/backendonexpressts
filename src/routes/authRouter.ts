import * as express from 'express';
// import { UserJwt } from '@/models/User';
import * as cookieParser from 'cookie-parser';
/* import {
  cacheControlMiddleware,
  parseAuthorizationMiddleware,
  endPoint,
} from './lib'; */

const authController = require('../controllers/authController')

const  authRouter = express.Router();


authRouter.use(express.urlencoded({ extended: false }));
authRouter.use(express.json());

// POST запросы

// locathost:5000/api/auth/reg
authRouter.post('/reg', authController.reg);
// locathost:5000/api/auth/login
authRouter.post('/login', authController.login);

module.exports = authRouter;